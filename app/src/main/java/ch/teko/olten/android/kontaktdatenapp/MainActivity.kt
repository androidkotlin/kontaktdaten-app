package ch.teko.olten.android.kontaktdatenapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import ch.teko.olten.android.kontaktdatenapp.model.Person
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val kontakte = ArrayList<Person>()

        kontakte.add(Person("Tom", 30, "ABC"))
        kontakte.add(Person("Alex", 30, "ABC"))
        kontakte.add(Person("Susi", 30, "ABC"))
        kontakte.add(Person("Sabrina", 30, "ABC"))
        kontakte.add(Person("Mario", 30, "ABC"))
        kontakte.add(Person("Luigi", 30, "ABC"))
        kontakte.add(Person("Susanne", 30, "ABC"))
        kontakte.add(Person("Tobi", 30, "ABC"))
        kontakte.add(Person("Gianni", 30, "ABC"))
        kontakte.add(Person("Miriam", 30, "ABC"))
        kontakte.add(Person("Luana", 30, "ABC"))
        kontakte.add(Person("Max", 30, "ABC"))

        val kontakteAdapter = PersonAdapter(this, kontakte)
        kontaktListe.adapter = kontakteAdapter

        kontaktListe.setOnItemClickListener { parent, view, position, id ->
            Toast.makeText(this, "Geklickt wurde: ${kontakte.get(position)}", Toast.LENGTH_SHORT).show()
        }
    }
}
