package ch.teko.olten.android.kontaktdatenapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import ch.teko.olten.android.kontaktdatenapp.model.Person

class PersonAdapter (context: Context, kontaktDaten:ArrayList<Person>) : BaseAdapter() {

    private val context:Context = context
    private val kontaktDaten = kontaktDaten

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val layoutInflater = LayoutInflater.from(context)
        val listenEintrag = layoutInflater.inflate(R.layout.kontakt_layout, parent, false)

        val nameTxt = listenEintrag.findViewById<TextView>(R.id.nameTxt)
        val alterTxt = listenEintrag.findViewById<TextView>(R.id.alterTxt)
        val bemerkungTxt = listenEintrag.findViewById<TextView>(R.id.bemerkungTxt)

        val person = kontaktDaten.get(position)

        nameTxt.text = person.name
        alterTxt.text = person.alter.toString()
        bemerkungTxt.text = person.bemerkung

        return listenEintrag
    }

    override fun getItem(position: Int): Any {
        return kontaktDaten.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return kontaktDaten.size
    }
}