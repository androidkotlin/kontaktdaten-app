package ch.teko.olten.android.kontaktdatenapp.model

data class Person(val name:String, val alter:Int, val bemerkung:String) {

    override fun toString(): String {
        return "$name ($alter)"
    }

}